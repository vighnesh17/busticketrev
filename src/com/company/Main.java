package com.company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.*;
import java.util.Scanner;

class Bus{
  private int busNo;
  private boolean ac;
  private int capacity;
  Bus(int busnum,boolean ac,int cap){
    this.busNo=busnum;
    this.ac=ac;
    this.capacity=cap;
  }

  public int getBusNo() {
    return busNo;
  }

  public int getCapacity() {
    return capacity;
  }

  public void setCapacity(int capacity) {
    this.capacity = capacity;
  }

  public  boolean isAc(){
    return ac;
  }

  public void setAc(boolean ac) {
    this.ac = ac;
  }
  public void displaybus(){
    System.out.println("bus no:"+busNo+" Ac:"+ac+" capacity:"+capacity);
  }
}
class Booking{
  String passengername;
  int age;
  int busNo;
  String gender;
  Date date;
  Booking()  {
    Scanner scanner=new Scanner(System.in);
    System.out.println("enter the name");
    passengername=scanner.next();
    System.out.println("enter the age");
    age=scanner.nextInt();
    System.out.println("enter the busno");
    busNo=scanner.nextInt();
    System.out.println("enter the gender");
    gender=scanner.next();
    System.out.println("enter the date");
    String dateInput= scanner.next();
    SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");

    try {
      date=dateFormat.parse(dateInput);
    } catch (ParseException e) {
      e.printStackTrace();
    }


  }
  public  boolean isavalable(ArrayList<Booking>bookings,ArrayList<Bus>buses){
    int capacity=0;
            for(Bus bus:buses){
              if(bus.getBusNo()== busNo)
                capacity=bus.getCapacity();
            }
            int booked=0;
            for (Booking b:bookings){
              if(b.busNo==busNo && b.date.equals(date)){
                booked++;
              }
            }
            return booked<capacity?true:false;
}
}
public class  Main {
  public static void main(String[] args) {
    ArrayList<Bus> buses = new ArrayList<Bus>();
    ArrayList<Booking> bookings = new ArrayList<Booking>();
    buses.add(new Bus(1, true, 2));
    buses.add(new Bus(2, false, 3));
    buses.add(new Bus(3, true, 4));

    int useroption = 1;
    Scanner scanner = new Scanner(System.in);
    for(Bus b:buses){
      b.displaybus();
    }
    while (useroption == 1) {
      System.out.println("enter 1 for booking 2 for exit");
      useroption = scanner.nextInt();
      if (useroption == 1) {
        Booking booking = new Booking();
        if (booking.isavalable(bookings, buses)) {
          bookings.add(booking);
          System.out.println("your booking is confirmed");
        }else {
          System.out.println("sorry booking is filled");
        }
      }
      }
    }
  }
